import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2

Item {
    property int grupa_id: -1

    ListModel
    {
        id:grupyModel
    }

    ListModel
    {
        id:wydzialyModel
    }

    function dodajGrupe(nazwaGrupy, idWydzialu)
    {
        var db = LocalStorage.openDatabaseSync("CaprioDB", "1.0", "", 1000000);

        db.transaction(
                    function(tx) {
                        console.log(nazwaGrupy + " " + idWydzialu)
                        var rs = tx.executeSql("INSERT INTO Grupa VALUES(NULL,?,?)",[nazwaGrupy,idWydzialu]);


                    }

                    )
    }

    function usunGrupe()
    {
        console.log(grupyModel.get(grupyTableView.currentRow).nazwaGrupy + " <<< nazwa")

        var db = LocalStorage.openDatabaseSync("CaprioDB", "1.0", "", 1000000);

        db.transaction(
                    function(tx) {
                        tx.executeSql("DELETE FROM Grupa WHERE nazwa = ?",[grupyModel.get(grupyTableView.currentRow).nazwaGrupy])
                    }
                    )

        grupyModel.remove(grupyTableView.currentRow)
    }

    Component.onCompleted: {
        var db = LocalStorage.openDatabaseSync("CaprioDB", "1.0", "", 1000000);

        db.transaction(
                    function(tx) {


                        //console.log(rs.insertId);

                        //var rs = tx.executeSql("SELECT w.nazwa wnazwa, g.nazwa gnazwa FROM Grupa AS g ");

                        if(grupa_id == -1)
                        {
                            var rs = tx.executeSql("SELECT g.id gid, g.nazwa gnazwa, w.nazwa wnazwa FROM Grupa AS g JOIN Wydzial AS w ON g.wydzial_id = w.id ORDER BY w.nazwa");
                        }
                        else
                            rs = tx.executeSql("SELECT g.id gid, g.nazwa gnazwa, w.nazwa wnazwa FROM Grupa AS g JOIN Wydzial AS w ON g.wydzial_id = w.id WHERE w.id = ? ORDER BY g.nazwa",[grupa_id])
                        for(var i = 0; i < rs.rows.length; i++)
                        {
                            var rs2 = tx.executeSql("SELECT Count(*) c FROM Student WHERE grupa_id = ?",[rs.rows.item(i).gid]);
                            grupyModel.append({"nazwaGrupy":rs.rows.item(i).gnazwa,"nazwaWydzialu":rs.rows.item(i).wnazwa, "liczbaStudentow":rs2.rows.item(0).c});
                        }

                        rs = tx.executeSql("SELECT * FROM Wydzial")
                        for(var i = 0; i < rs.rows.length; i++)
                        {
                            wydzialyModel.append({"text":rs.rows.item(i).nazwa, "idx":rs.rows.item(i).id})
                        }

                    })
    }


    Row
    {
        id:toolBar
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 12
        height: 50
        spacing: 6
        Button
        {
            width: 100
            anchors.top:parent.top
            anchors.bottom: parent.bottom
            text:"Nowa grupa"
            onClicked: nowaGrupaDialog.open()
        }
        Button
        {
            width: 100
            anchors.top:parent.top
            anchors.bottom: parent.bottom
            text:"Usuń grupę"
            enabled: grupyTableView.currentRow == -1 ? false : true
            onClicked: usunGrupe()
        }

    }

    TableView
    {
        id:grupyTableView
        anchors.top:toolBar.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 12
        TableViewColumn {
            role: "nazwaGrupy"
            title: "Nazwa grupy"
            width: 100
        }
        TableViewColumn {
            role: "nazwaWydzialu"
            title: "Nazwa wydziału"
            width: 200
        }
        TableViewColumn {
            role:"liczbaStudentow"
            title:"Liczba Studentów"
            width: 200
        }
        model:grupyModel
        section.property: "nazwaWydzialu"
        section.delegate: sectionHeading
    }

    Component {
        id: sectionHeading
        Rectangle {
            width: parent.width
            height: childrenRect.height*2
            color:"lightgrey"
            Text {
                anchors.centerIn: parent
                text: section
                font.bold: true
                font.pixelSize: 20
            }
        }
    }
    Dialog {
        id: nowaGrupaDialog
        visible: false
        width: 300
        height: 250
        title: "Nowa grupa"
        standardButtons: StandardButton.Save | StandardButton.Cancel
        onAccepted: dodajGrupe(nazwaField.text, wydzialyModel.get(comboWybierzWydzial.currentIndex).idx)

        Column
        {
            anchors.fill: parent
            anchors.margins: 12
            Label
            {
                text: "Podaj nazwę grupy:"
            }

            TextField
            {
                id:nazwaField
                width: parent.width
            }
            Item
            {
                height: 20
                width: 100
            }

            Label
            {
                text:"Wybierz wydział:"
            }
            ComboBox
            {
                id:comboWybierzWydzial
                model: wydzialyModel
                width: parent.width
            }
        }
    }
}

