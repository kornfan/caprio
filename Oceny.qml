import QtQuick 2.0
import QtQuick.Controls 1.4

Item {

    TabView
    {
        id:tabView
        anchors.fill: parent
        anchors.margins: 12

        Tab{
            title:"Ala"

            TableView
            {
                anchors.fill: parent
                anchors.margins: 12
            }
            Button
            {
                onClicked:{
                        var c = Qt.createComponent("Semestr.qml");
                        tabView.addTab("tab", c);
                        var last = tabView.count-1;
                        tabView.getTab(last).active = true; // Now the content will be loaded
                        tabView.getTab(last).item.sModel = testModel
                        console.log(tabView.getTab(last).item);
                    }
            }
        }
        Tab{
            title:"ma"
            Rectangle {
                color:"lightpink"
            }
        }
        Tab{
            title:"kota"
            Rectangle {
                color:"lightgrey"
            }
        }

    }
}

