import QtQuick 2.3
import QtQuick.Window 2.2
import QtQuick.Layouts 1.1
import QtQuick.Controls 1.4
import QtQuick.Controls.Styles 1.4
import QtQuick.LocalStorage 2.0

Window {
    visible: true
    width: 1024
    height: 768
    minimumHeight: 600
    minimumWidth: 800
    color:"white"

    function dropDB() {
        var db = LocalStorage.openDatabaseSync("CaprioDB", "1.0", "",1000000)
        db.transaction(
                    function(tx)
                    {
                        tx.executeSql('DROP VIEW Przedmioty_sem');

                        tx.executeSql('DROP TABLE Grupa');;

                        tx.executeSql('DROP TABLE Kurs');

                        tx.executeSql('DROP TABLE Przedmiot');

                        tx.executeSql('DROP TABLE Semestr');

                        tx.executeSql('DROP TABLE Semestr_Kurs');

                        tx.executeSql('DROP TABLE Student');

                        tx.executeSql('DROP TABLE Wydzial');

                        tx.executeSql('DROP TABLE Wykladowca;')
                    }
                    )
    }

    function initDB() {
        var db = LocalStorage.openDatabaseSync("CaprioDB", "1.0", "", 1000000);

        db.transaction(
                    function(tx) {

                        tx.executeSql('CREATE TABLE IF NOT EXISTS Grupa(
    id INTEGER PRIMARY KEY,
    nazwa varchar(255) UNIQUE  NOT NULL,
    wydzial_id int  NOT NULL,
    FOREIGN KEY (wydzial_id) REFERENCES Wydzial (id) ON DELETE RESTRICT
)');


                        tx.executeSql('
CREATE TABLE IF NOT EXISTS Kurs (
    id INTEGER PRIMARY KEY,
    przedmiot_id int  NOT NULL,
    wykladowca_id int  NOT NULL,
    FOREIGN KEY (przedmiot_id) REFERENCES Przedmiot (id),
    FOREIGN KEY (wykladowca_id) REFERENCES Wykladowca (id)
)');

                        tx.executeSql('
CREATE TABLE IF NOT EXISTS Przedmiot (
    id INTEGER PRIMARY KEY,
    nazwa varchar(255)  NOT NULL,
    ects int  NOT NULL
)');

                        tx.executeSql('
CREATE TABLE IF NOT EXISTS Semestr (
    id INTEGER PRIMARY KEY,
    student_id int  NOT NULL,
    data_rozp date  NOT NULL,
    data_zak date  NOT NULL,
    FOREIGN KEY (student_id) REFERENCES Student (id)
)');

                        tx.executeSql('
CREATE TABLE IF NOT EXISTS Semestr_Kurs (
    semestr_id INTEGER  NOT NULL,
    kurs_id INTEGER  NOT NULL,
    ocena decimal(2,2)  NOT NULL,
    FOREIGN KEY (semestr_id) REFERENCES Semestr (id),
    FOREIGN KEY (kurs_id) REFERENCES Kurs (id)
)');

                        tx.executeSql('
CREATE TABLE IF NOT EXISTS Student (
    id INTEGER PRIMARY KEY,
    imie_nazwisko varchar(255)  NOT NULL,
    data_ur date  NOT NULL,
    nr_indeksu INTEGER  NOT NULL,
    pesel character(11)  NOT NULL,
    grupa_id INTEGER  NOT NULL,
    FOREIGN KEY (grupa_id) REFERENCES Grupa (id) ON DELETE RESTRICT
)');

                        tx.executeSql('
CREATE TABLE IF NOT EXISTS Wydzial (
    id INTEGER PRIMARY KEY,
    nazwa varchar(255)  NOT NULL
)');

                        tx.executeSql('
CREATE TABLE IF NOT EXISTS Wykladowca (
    id INTEGER  PRIMARY KEY,
    imie_nazwisko varchar(255)  NOT NULL,
    email varchar(255)  NOT NULL
)');


                        tx.executeSql('
CREATE VIEW IF NOT EXISTS Przedmioty_sem AS
SELECT p.nazwa, sk.ocena, s.student_id FROM Semestr_Kurs AS sk
JOIN Kurs AS k ON sk.kurs_id = k.id
JOIN Przedmiot AS p ON p.id = k.przedmiot_id
JOIN Semestr AS s ON sk.semestr_id=s.id;');

                        // Show all added greetings
                        //                        var rs = tx.executeSql('SELECT * FROM Greeting');

                        //                        var r = ""
                        //                        for(var i = 0; i < rs.rows.length; i++) {
                        //                            r += rs.rows.item(i).salutation + ", " + rs.rows.item(i).salutee + "\n"
                        //                        }
                        //                        text = r
                    }
                    )
    }
    Component.onCompleted: {
        //dropDB();
        //initDB();
    }

    ListModel {
        id: testModel
        ListElement{
            przedmiot:"analiza"
            ocena:5
        }
    }

    SplitView
    {
        anchors.fill: parent

        Item    {
            id:leftBar
            Layout.minimumWidth: 200
            Layout.maximumWidth: 400
            Column
            {
                anchors.fill: parent
                anchors.margins: 12
                spacing: 6
                //// Oceny Button
                Button
                {
                    height: parent.height/10
                    width: parent.width
                    text:"Oceny"
                    onClicked: pageLoader.source = "Oceny.qml"
                }
                //// Wydziały Button
                Button
                {
                    height: parent.height/10
                    width: parent.width
                    text:"Wydziały"
                    onClicked: pageLoader.source = "Wydzialy.qml"
                }
                Button
                {
                    height: parent.height/10
                    width: parent.width
                    text:"Grupy"
                    onClicked: {
                        pageLoader.setSource("Grupy.qml",{"grupa_id":-1})
                    }
                }
            }
        }
        Item
        {
            Layout.minimumWidth: 200
            Layout.fillWidth: true
            Loader {
                id: pageLoader
                anchors.fill: parent
                source: "Oceny.qml"
            }


        }
    }
}

