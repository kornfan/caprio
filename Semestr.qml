import QtQuick 2.0
import QtQuick.Controls 1.4

Item  {
    property ListModel sModel
    TableView {
        anchors.fill:parent
        anchors.margins: 12
        TableViewColumn {
            role: "przedmiot"
            title: "Przedmiot"
            width: 100
        }
        TableViewColumn {
            role: "ocena"
            title: "Ocena"
            width: 200
        }
        model: sModel
    }

}

