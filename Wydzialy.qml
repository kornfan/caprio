import QtQuick 2.0
import QtQuick.LocalStorage 2.0
import QtQuick.Dialogs 1.2
import QtQuick.Controls 1.4

Item {

    function insertWydzial(index, nazwa)
    {
        var db = LocalStorage.openDatabaseSync("CaprioDB", "1.0", "", 1000000);
        db.transaction(
                    function(tx) {
                        tx.executeSql("INSERT INTO Wydzial VALUES(NULL,?)",[nazwa]);

                    })
        wydzialyModel.insert(index,{"nazwa":nazwa});
    }
    
    ListModel
    {
        id:wydzialyModel
        ListElement
        {
            idx:2
            nazwa:"Wydział Cybernetyki"
        }
        ListElement
        {
            idx:-1
            nazwa:"Wydział Chemii"
        }
    }

    Component.onCompleted:
    {
        var db = LocalStorage.openDatabaseSync("CaprioDB", "1.0", "", 1000000);

        db.transaction(
                    function(tx) {
                        var rs = tx.executeSql("SELECT * from Wydzial");

                        for(var i = 0; i < rs.rows.length; i++)
                        {
                            wydzialyModel.append({"idx": rs.rows.item(i).id,"nazwa":rs.rows.item(i).nazwa});

                        }
                        wydzialyModel.append({"nazwa":"Dodaj nowy"});
                    })
    }


    Component {
            id: wydzialyDelegate
            Item {
                width: wydzialGrid.cellWidth; height: wydzialGrid.cellHeight
                Rectangle {
                    anchors.fill: parent
                    anchors.margins: 10
                    radius: 5
                    color: "lightgrey"
                    Text { text: nazwa; anchors.fill: parent; horizontalAlignment: Text.AlignHCenter; verticalAlignment: Text.AlignVCenter; wrapMode: Text.WordWrap }
                    MouseArea
                    {
                        anchors.fill:parent
                        onClicked: {
                            if(index == wydzialyModel.count - 1)
                            {
                                wydzialGrid.currentIndex = index;
                                nowyWydzialDialog.open();
                            }
                            else
                            {
                                pageLoader.setSource("Grupy.qml",{"grupa_id":idx})
                            }
                        }
                    }
                }
            }
        }

    GridView
    {
        id:wydzialGrid
        cellWidth: parent.width/5
        cellHeight: parent.height/6

        anchors.fill: parent
        model:wydzialyModel
        delegate: wydzialyDelegate
        focus: true

    }

    Dialog {
        id: nowyWydzialDialog
        visible: false
        width: 300
        height: 250
        title: "Nowy wydział"
        standardButtons: StandardButton.Save | StandardButton.Cancel
        onAccepted: insertWydzial(wydzialGrid.currentIndex, nazwaField.text)

        TextField
        {
            id:nazwaField
            anchors.left: parent.left
            anchors.right: parent.right
            text: "Podaj nazwę"
        }
    }
}

